<?php

class Autoload
{
	public static function load_class($class_name)
	{
		//echo $class_name;
		$class_path = str_replace("\\", '/', $class_name);
		$class_path = __DIR__ . "\\" . $class_path . ".php";
		//echo $class_path;
		if (file_exists($class_path)) {
			require_once $class_path;
		} else {
			echo("No such file");

		}
	}
}