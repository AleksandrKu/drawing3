<?php
//
require_once "Autoload.php";
spl_autoload_register("Autoload::load_class");
$circle = new \shapes\Circle();
echo $circle->draw();
$rectangle = new \shapes\Rectangle();
echo $rectangle->draw();
$square = new \shapes\Square();
echo $square->draw();
$triangle = new \shapes\Triangle();
echo $triangle->draw();

