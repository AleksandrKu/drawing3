<?php
namespace shapes;
class Square extends Shape
{
	function __construct()
	{
		$this->color = "red";
		$this->width = "100px";
		$this->height = "100px";
	}

	function draw()
	{

		echo "Квадрат";
		echo "<div style=\"width: {$this->width}; height: {$this->height}; background: {$this->color};\"></div>";
	}
}