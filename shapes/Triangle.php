<?php
namespace shapes;
class Triangle extends Shape
{
	function __construct()
	{
		$this->color = "red";
		$this->width = "0px";
		$this->height = "0px";
	}

	function draw()
	{
		echo "Треугольник";
		echo "<div style=\"width: {$this->width}; height: {$this->height}; border-left: 50px solid transparent; border-right: 50px solid transparent; border-bottom: 100px solid {$this->color};\"></div>";
	}
}