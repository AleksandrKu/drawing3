<?php
namespace shapes;
class Circle extends Shape
{
	function __construct()
	{
		$this->color = "red";
		$this->width = "100px";
		$this->height = "100px";
	}

	function draw()
	{

		echo "Круг";
		echo "<div style='width: {$this->width}; height: {$this->height}; background: {$this->color}; -moz-border-radius: 50px; -webkit-border-radius: 50px; border-radius: 50px;'></div>";
	}


}