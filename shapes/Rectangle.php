<?php
namespace shapes;
class Rectangle extends Shape
{
	function __construct()
	{
		$this->color = "red";
		$this->width = "200px";
		$this->height = "100px";
	}

	function draw()
	{

		echo "Прямоугольник";
		echo "<div style=\"width: {$this->width}; height: {$this->height}; background: {$this->color};\"></div>";
	}
}