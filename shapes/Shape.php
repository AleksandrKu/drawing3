<?php
namespace shapes;
abstract class Shape
{
	protected $color;
	protected $width;
	protected $height;

	public abstract function draw();
}